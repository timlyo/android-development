package com.timlyo.waitersfriend;

import com.timlyo.waitersfriend.database.DBHelper;

public class Init {
    public static void addItems(DBHelper database){
//        database.recreate();

        database.addEntry(new MenuItem("Bagel", "1.50", MenuItem.Category.STARTER));
        database.addEntry(new MenuItem("Waffle", "1.50", MenuItem.Category.STARTER));

        database.addEntry(new MenuItem("Steak", "5.00", MenuItem.Category.MAIN, true));
        database.addEntry(new MenuItem("Burger", "5.00", MenuItem.Category.MAIN, true));
        database.addEntry(new MenuItem("Fish and Chips", "5.00", MenuItem.Category.MAIN));

        database.addEntry(new MenuItem("Cola", "1.50", MenuItem.Category.DRINK));
        database.addEntry(new MenuItem("Sprite", "1.50", MenuItem.Category.DRINK));
    }
}
