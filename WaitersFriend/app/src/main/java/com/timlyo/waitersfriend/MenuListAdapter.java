package com.timlyo.waitersfriend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MenuListAdapter extends BaseAdapter{
    List<MenuItem> items = new ArrayList<MenuItem>();

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    /*
    More specific version for this app
     */
    public MenuItem getMenuItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Context context = MenuActivity.getAppContext();

        try{
            MenuItem item = items.get(position);

            if(view == null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.item_layout, null);
            }

            TextView itemName = (TextView) view.findViewById(R.id.itemName);
            TextView itemPrice = (TextView) view.findViewById(R.id.itemPrice);
            TextView itemCategory = (TextView) view.findViewById(R.id.itemCategory);

            itemName.setText(item.getName());
            itemPrice.setText(item.getFormattedPrice());
            itemCategory.setText(item.getCategory());
        }catch (Exception e){
            e.printStackTrace();
            e.getCause();
        }

        return view;
    }

    public void addItem(MenuItem item){
        items.add(item);
        this.notifyDataSetChanged();
    }


}
