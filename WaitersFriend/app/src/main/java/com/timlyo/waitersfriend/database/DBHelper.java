package com.timlyo.waitersfriend.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.timlyo.waitersfriend.MenuActivity;
import com.timlyo.waitersfriend.MenuItem;
import com.timlyo.waitersfriend.MenuListAdapter;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "menu";
    private static int DATABASE_VERSION = 1;
    private SQLiteDatabase database;


    public DBHelper() {
        super(MenuActivity.getAppContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(){
        onCreate(database);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        if(database.isReadOnly()){
            database.close();
            database = this.getWritableDatabase();
        }
        String command = "CREATE TABLE " + DATABASE_NAME +  "(name text primary key, price TEXT, category TEXT, cookTime BOOLEAN);";
        database.execSQL(command);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(DBHelper.class.getName(), "Upgrading database");
        database.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
        onCreate(database);
    }

    public void addEntry(MenuItem item){
        ContentValues values = new ContentValues();
        values.put("name", item.getName());
        values.put("price", item.getStringPrice());
        values.put("category", item.getCategory());
        values.put("cookTime", item.isCookTimeSetting());
        this.getWritableDatabase().insert(DATABASE_NAME, null, values);
    }

    public void loadMenu(MenuListAdapter menuList){
        String[] columns = {"name", "price", "category", "cookTime"};
        database = this.getReadableDatabase();
        Cursor cursor = database.query(DATABASE_NAME, columns, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            String name = cursor.getString(0);
            String price = cursor.getString(1);
            String category = cursor.getString(2);
            boolean cookTime = cursor.getInt(3) != 0;
            MenuItem menuItem = new MenuItem(name, price, category, cookTime);
            menuList.addItem(menuItem);
            cursor.moveToNext();
        }
        cursor.close();
        this.close();
    }

    public void recreate(){
        this.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME + ";");
        onCreate();
    }
}
