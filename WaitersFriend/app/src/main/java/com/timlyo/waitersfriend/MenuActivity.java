package com.timlyo.waitersfriend;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.timlyo.waitersfriend.database.DBHelper;


public class MenuActivity extends ActionBarActivity {
    private static Context appContext;

    private DBHelper databaseHelper;

    private MenuListAdapter menuListAdapter;
    private ListView menuList;

    private static OrderAdapter orderAdapter;

    public static com.timlyo.waitersfriend.MenuItem currentItem;

    private static TextView countBox;
    private static Button checkoutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //class pointers
        appContext = getApplicationContext();
        databaseHelper = new DBHelper();
        orderAdapter = new OrderAdapter();

        //interface
        setupInterface();

        //data
        //Init.addItems(databaseHelper);
        databaseHelper.loadMenu(menuListAdapter);
    }

    public static void updateCountBox(int count){
        countBox.setText("Number of Items:" + count);
    }

    protected void setupInterface(){
        menuList = (ListView)findViewById(R.id.menuList);
        menuListAdapter = new MenuListAdapter();
        menuList.setAdapter(menuListAdapter);

        countBox = (TextView) findViewById(R.id.countBoxField);
        checkoutButton = (Button) findViewById(R.id.checkoutButton);

        menuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentItem = menuListAdapter.getMenuItem(position);
                Intent intent = new Intent(getAppContext(), OrderActivity.class);
                startActivity(intent);
            }
        });

        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getAppContext(), CheckOutActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static Context getAppContext(){
        return appContext;
    }

    public static OrderAdapter getOrderAdapter(){
        return orderAdapter;
    }
    public static com.timlyo.waitersfriend.MenuItem getCurrentItem(){
        return currentItem;
    }
}
