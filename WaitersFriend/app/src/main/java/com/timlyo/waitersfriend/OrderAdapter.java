package com.timlyo.waitersfriend;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OrderAdapter extends BaseAdapter{
    List<MenuItem> items = new ArrayList<MenuItem>();

    public void addItem(MenuItem item){
        items.add(item);
        MenuActivity.updateCountBox(getCount());
    }

    @Override
    public int getCount(){
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Context context = MenuActivity.getAppContext();

        try{
            MenuItem item = items.get(position);

            if(view == null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.checkout_item_layout, null);
            }

            TextView itemName = (TextView) view.findViewById(R.id.checkoutItemName);
            TextView itemPrice = (TextView) view.findViewById(R.id.checkoutItemPrice);
            TextView itemOptions = (TextView) view.findViewById(R.id.checkOutItemOptions);

            itemName.setText(item.getName());
            itemPrice.setText(item.getFormattedPrice());

            List<String> options = new ArrayList<String>();
            String optionString = ""; //formatted string to output options
            if(item.getCategory().equals(MenuItem.Category.DRINK)) {
                if (item.isIce())
                    options.add("Ice");
                if (item.getSize() != null)
                    options.add(item.getSize().name());

            }else if(item.getCategory().equals(MenuItem.Category.STARTER)) {
                if(item.isArriveWithMeal())
                    options.add("Arrive with meal");

            }else if(item.getCategory().equals(MenuItem.Category.MAIN)) {
                if (item.isCookTimeSetting())
                    options.add(item.getCookTime());

                if (item.getSide() != null) {
                    String side = item.getSide().name();
                    if(!side.equals("none"))
                        options.add(side);
                }
            }

            //iterate through options and add to string
            for(int i = 0; i < options.size(); i++){
                String option = options.get(i);
                if(optionString.length() > 0)
                    if(i != 0)
                        optionString += ", ";
                if (option.length() > 0)
                    optionString += option;
            }


            itemOptions.setText(optionString);
        }catch (Exception e){
            e.printStackTrace();
            e.getCause();
        }

        return view;
    }

    public BigDecimal getTotalCost(){
        BigDecimal total = new BigDecimal("0");
        for(int i = 0; i < items.size(); i++){
            total = total.add(new BigDecimal(items.get(i).getStringPrice()));
        }
        return total;
    }

    public String getTotalCostFormattedString(){
        return "Total: £" + getTotalCost().toPlainString();
    }
}
