package com.timlyo.waitersfriend;

import java.math.BigDecimal;

public class MenuItem {
    private String name;
    private BigDecimal price;
    private String category;

    private boolean ice;
    private Size size;
    private Side side;

    private boolean cookTimeSetting;
    private CookTime cookTime;
    private boolean arriveWithMeal;

    public MenuItem(String name, String price, String category, boolean cookTimeSetting){
        setName(name);
        setPrice(price);
        setCategory(category);

        this.cookTimeSetting = cookTimeSetting;
    }

    public MenuItem(String name, String price, String category){
        setName(name);
        setPrice(price);
        setCategory(category);

        cookTimeSetting = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getStringPrice() {
        return price.toPlainString();
    }
    public String getFormattedPrice(){
        return "£" + price.toPlainString();
    }

    public void setPrice(String price){
        this.price = new BigDecimal(price);
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isIce() {
        return ice;
    }

    public boolean isArriveWithMeal() {
        return arriveWithMeal;
    }

    public void setArriveWithMeal(boolean value){
        arriveWithMeal = value;
    }

    public void setIce(boolean ice) {
        this.ice = ice;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Side getSide() {
        return side;
    }

    public void setSide(Side side) {
        this.side = side;
    }

    public boolean isCookTimeSetting() {
        return cookTimeSetting;
    }

    public void setCookTime(CookTime cookTime){
        this.cookTime = cookTime;
    }

    public String getCookTime(){
        return cookTime.name();
    }

    public class Category{
        public final static String MAIN = "main";
        public final static String STARTER = "starter";
        public final static String DRINK = "drink";
    }

    public enum Size{
        large, medium, small
    }

    public enum Side{
        chips, peas, none
    }

    public enum CookTime{
        veryRare, rare, medium, wellDone, charcoal
    }
}
