package com.timlyo.waitersfriend;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import java.math.BigDecimal;


public class OrderActivity extends ActionBarActivity implements DrinkOrderFragment.OnFragmentInteractionListener, MainCourseOrderFragment.OnFragmentInteractionListener, StarterOrderFragment.OnFragmentInteractionListener{
    private String category;

    com.timlyo.waitersfriend.MenuItem item;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        item = MenuActivity.getCurrentItem();

        Intent intent = getIntent();
        category = intent.getStringExtra("category");
        setupInterface();
    }

    private void setupInterface(){
        FragmentManager manager = getFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        //hide non relevant fragments
        if(item.getCategory().equals("drink")){
            ft.hide(manager.findFragmentById(R.id.mainCourseFragment));
            ft.hide(manager.findFragmentById(R.id.starterOrderFragment));

        }else if(item.getCategory().equals("main")){
            ft.hide(manager.findFragmentById(R.id.drinksOrderFragment));
            ft.hide(manager.findFragmentById(R.id.starterOrderFragment));

        }else if(item.getCategory().equals("starter")){
            ft.hide(manager.findFragmentById(R.id.mainCourseFragment));
            ft.hide(manager.findFragmentById(R.id.drinksOrderFragment));
        }

        ft.commit();

        //register cancel button
        findViewById(R.id.orderCancelButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //register add button
        findViewById(R.id.orderAddButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //side checker
                RadioGroup sidesGroup = (RadioGroup)findViewById(R.id.sidesRadioGroup);
                if(sidesGroup.getCheckedRadioButtonId() > 0) {
                    int radioButtonId = sidesGroup.getCheckedRadioButtonId();
                    View radioButton = sidesGroup.findViewById(radioButtonId);
                    int value = sidesGroup.indexOfChild(radioButton);
                    com.timlyo.waitersfriend.MenuItem.Side side = com.timlyo.waitersfriend.MenuItem.Side.values()[value];
                    item.setSide(side);
                }

                CheckBox iceBox = (CheckBox)findViewById(R.id.ice);
                item.setIce(iceBox.isChecked());

                CheckBox arriveWithMealBox = (CheckBox) findViewById(R.id.arriveWithMealBox);
                item.setArriveWithMeal(arriveWithMealBox.isChecked());

                SeekBar cookTimeBar = (SeekBar)findViewById(R.id.cookTimeBar);
                int cookValue = cookTimeBar.getProgress();
                if(cookValue < 25)
                    item.setCookTime(com.timlyo.waitersfriend.MenuItem.CookTime.veryRare);
                else if(cookValue < 50)
                    item.setCookTime(com.timlyo.waitersfriend.MenuItem.CookTime.rare);
                else if(cookValue < 75)
                    item.setCookTime(com.timlyo.waitersfriend.MenuItem.CookTime.medium);
                else if(cookValue < 100)
                    item.setCookTime(com.timlyo.waitersfriend.MenuItem.CookTime.wellDone);
                else if(cookValue < 125)
                    item.setCookTime(com.timlyo.waitersfriend.MenuItem.CookTime.charcoal);


                RadioGroup sizeGroup = (RadioGroup)findViewById(R.id.sizeRadioGroup);

                //size checker
                if(sizeGroup.getCheckedRadioButtonId() > 0) {
                    int radioButtonId = sizeGroup.getCheckedRadioButtonId();
                    View radioButton = sizeGroup.findViewById(radioButtonId);
                    int value = sizeGroup.indexOfChild(radioButton);
                    com.timlyo.waitersfriend.MenuItem.Size size = com.timlyo.waitersfriend.MenuItem.Size.values()[value];
                    item.setSize(size);
                }

                MenuActivity.getOrderAdapter().addItem(item);
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        System.out.println("Fragment interaction");
    }
}
