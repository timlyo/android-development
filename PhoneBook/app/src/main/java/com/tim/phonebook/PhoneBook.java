package com.tim.phonebook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

public class PhoneBook extends Activity {
    private static PhoneListAdapter listAdapter;
    protected ListView listView;

    private static Context context;
    private DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.setupInterface();

        this.dataManager = new DataManager(this);
        this.dataManager.loadData(listAdapter);

    }

    /*
    function to register buttons, lists and other objects to do with the user interface
     */
    protected void setupInterface(){
        listView = (ListView)findViewById(R.id.listView);
        listAdapter = new PhoneListAdapter();

        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getAppContext(), ViewEntryActivity.class);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });

        PhoneBook.context = getApplicationContext();
    }

    public static Context getAppContext(){
        return PhoneBook.context;
    }
    public static PhoneListAdapter getListObject(){
        return listAdapter;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add) {
            Intent intent = new Intent(this, AddActivity.class);

            startActivityForResult(intent, 1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_CANCELED)
            return;

        if(requestCode == 1){
            String name = data.getStringExtra("name");
            String phone = data.getStringExtra("phone");

            listAdapter.addItem(name, phone);
            listAdapter.sort();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dataManager.saveEntries(listAdapter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        dataManager.saveEntries(listAdapter);
    }
}
