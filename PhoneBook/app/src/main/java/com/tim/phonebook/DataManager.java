package com.tim.phonebook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataManager extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 1;
    private static String databaseName = PhoneBook.getAppContext().getString(R.string.databaseFile);

    private SQLiteDatabase database;

    public DataManager(Context context) {
        super(context, databaseName, null, DATABASE_VERSION);
        this.onCreate(this.getWritableDatabase());
    }

    public void saveEntries(PhoneListAdapter phoneBook){
        this.clearData();
        database = this.getWritableDatabase();
        for(int i = 0; i < phoneBook.getCount(); i++){
            ContentValues contentValues = new ContentValues();

            PhoneListAdapter.Entry entry = phoneBook.getEntry(i);
            contentValues.put("name", entry.getName());
            contentValues.put("phone", entry.getPhone());

            database.insert(databaseName, null, contentValues);
        }
        database.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if(db.isReadOnly()){
            db.close();
            db = this.getWritableDatabase();
        }
        String structure = "CREATE TABLE " + databaseName + "(id integer primary key autoincrement, name, phone);";
        try {
            db.execSQL(structure);
        }catch (android.database.SQLException e){
            //noop
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DataManager.class.getName(), "Upgrading database from " + oldVersion + " to " + newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + databaseName);
        onCreate(db);
    }

    public void loadData(PhoneListAdapter adapter){
        database = this.getReadableDatabase();

        String[] columns = {"name", "phone"};

        Cursor cursor = database.query(
                databaseName, columns,
                null, null, null, null, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            String name = cursor.getString(0);
            String phone = cursor.getString(1);
            adapter.addItem(name, phone);
            cursor.moveToNext();
        }
    }

    public void clearData(){
        Log.v("DEBUG", "Cleared database");
        database = this.getWritableDatabase();
        database.execSQL("DROP TABLE IF EXISTS " + databaseName);
        this.onCreate(this.getWritableDatabase());
    }
}
