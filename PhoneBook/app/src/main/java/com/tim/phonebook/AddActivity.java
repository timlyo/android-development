package com.tim.phonebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class AddActivity extends Activity {
    private Button addButton;
    private Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        addButton = (Button) findViewById(R.id.addMenuAdd);
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                close();
            }
        });

        cancelButton = (Button) findViewById(R.id.addMenuCancel);
        cancelButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                cancel();
            }
        });
    }

    /*
        called when adding details if cancelled
     */
    protected void cancel(){
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    /*
        Called for successful adding of entry details
     */
    protected void close(){
        Intent intent = getIntent();

        EditText nameField = (EditText) findViewById(R.id.addMenuNameField);
        EditText phoneField = (EditText) findViewById(R.id.addMenuPhoneField);

        String name = nameField.getText().toString();
        if (name.length() == 0){ // check for empty field and warn user instead of closing
            String message = "name must not be empty";
            Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
            toast.show();

            return;
        }else{
            intent.putExtra("name", name);
        }

        String phone = phoneField.getText().toString();
        intent.putExtra("phone", phone);

        setResult(RESULT_OK, intent);

        finish();
    }
}
