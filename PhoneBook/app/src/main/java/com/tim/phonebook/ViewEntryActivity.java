package com.tim.phonebook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class ViewEntryActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_entry);

        setup();
    }

    protected void setup(){
        TextView phoneField = (TextView)findViewById(R.id.viewPhone);
        Button deleteButton = (Button)findViewById(R.id.viewDeleteButton);

        Intent intent = getIntent();

        final int position = intent.getIntExtra("position", -1);

        if(position == -1){
            finish();
        }

        final PhoneListAdapter.Entry entry = PhoneBook.getListObject().getEntry(position);

        setTitle(entry.getName());
        phoneField.setText(entry.getPhone());

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhoneBook.getListObject().deleteItem(position);

                Toast toast = Toast.makeText(PhoneBook.getAppContext(), entry.getName() + " has been deleted", Toast.LENGTH_LONG);
                toast.show();

                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_entry, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
