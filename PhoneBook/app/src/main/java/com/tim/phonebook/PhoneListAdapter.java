package com.tim.phonebook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class PhoneListAdapter extends BaseAdapter implements android.widget.ListAdapter {
    private List<Entry> nameList;

    public void addItem(String name, String phone){
        nameList.add(new Entry(name, phone));
        this.notifyDataSetChanged();
    }

    public PhoneListAdapter(){
        nameList = new ArrayList<Entry>();
    }


    @Override
    public int getCount() {
        if (nameList == null)
            return 0;
        return nameList.size();
    }

    @Override
    public Object getItem(int position) {
        if(position > 0 || position < this.getCount())
            return nameList.get(position);
        return null;
    }

    /*
    Program specific way to return only the entry object
     */
    public Entry getEntry(int position) {
        if(position > 0 || position < this.getCount())
            return nameList.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        sort(); // sort phoneList
        try{
            if(view == null){
                Context context = PhoneBook.getAppContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.list_layout, null);
            }

            TextView name = (TextView) view.findViewById(R.id.listName);
            TextView phone = (TextView) view.findViewById(R.id.listPhone);

            name.setText(nameList.get(position).getName());
            phone.setText(nameList.get(position).getPhone());

        }catch (Exception e){
            e.printStackTrace();
            e.getCause();
        }

        return view;
    }

    public void deleteItem(int position){
        nameList.remove(position);
        this.notifyDataSetChanged();
    }

    /*
        Class to store the details of the user
     */
    public class Entry{
        private String name;
        private String phone;

        public Entry(String name, String phone){
            this.name = name;
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public String getPhone() {
            return phone;
        }
    }

    public void sort(){
        //simple bubblesort is easiest and generally quickest for small data
        boolean swapped = true;
        int n = nameList.size() - 1;
        while (swapped) {
            swapped = false;
            for (int i = 0; i < n; i++) {
                if (nameList.get(i).getName().compareTo(nameList.get(i + 1).getName()) > 0) { // if 2nd string is "larger"
                    Entry temp = nameList.get(i); //swap entries
                    nameList.set(i, nameList.get(i + 1));
                    nameList.set(i + 1, temp);
                    swapped = true;
                }
                n--;
            }
        }
    }
}
