package com.timlyo.weather.api;


import com.timlyo.weather.Database;
import com.timlyo.weather.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class Parser {
    JSONObject jsonRoot;
    JSONObject sys;
    JSONObject weather;
    JSONObject main;
    JSONObject wind;
    JSONObject clouds;

    String weatherType;
    String baseName;
    String cityName;

    public Parser(String data){
        parseData(data);
        saveData();
    }

    protected void parseData(String data){
        try {
            jsonRoot = new JSONObject(data);

            sys = jsonRoot.getJSONObject("sys");
            weather = jsonRoot.getJSONArray("weather").getJSONObject(0);
            main = jsonRoot.getJSONObject("main");
            wind = jsonRoot.getJSONObject("wind");
            clouds = jsonRoot.getJSONObject("clouds");


            weatherType = weather.getString("description");
            baseName =  jsonRoot.getString("base");
            cityName = jsonRoot.getString("name");

        }catch (JSONException e){
            e.printStackTrace();
            e.getCause();
        }
    }

    protected void saveData(){
        Database database = MainActivity.getDatabase();
        database.saveValue("weatherType", weatherType);
        database.saveValue("baseName", baseName);
        database.saveValue("cityName", cityName);
    }
}
