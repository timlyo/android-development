package com.timlyo.weather;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.timlyo.weather.api.Locator;
import com.timlyo.weather.api.Parser;
import com.timlyo.weather.api.WeatherRequester;


public class MainActivity extends Activity {
    public static final String TAG = "MyActivity"; //tag for logging

    private static Context appContext;
    private static Database database;
    private static final String DATABASE_NAME = "weatherData";

    private static MainActivity instance;

    WeatherRequester requester;
    Parser parser;
    Locator locator;

    TextView weatherOverview;
    TextView baseName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appContext = getApplicationContext();

        database = new Database(DATABASE_NAME, this);
        instance = this;
        locator = new Locator();

        setupInterfaceObjects();

        //System.out.println(locator.getLocation().getLatitude());

        updateWeather();
    }

    public void updateWeather(){
        requester = new WeatherRequester();
        requester.setLatitude(locator.getlatitude());
        requester.setLongitude(locator.getLongitude());
        requester.execute();

        while(! requester.isDone()) { // wait until request is processed
            //noop
        }

        parser = new Parser(requester.getResponse());

        weatherOverview.setText(database.getValue("weatherType"));
        baseName.setText(database.getValue("baseName") + " at " + database.getValue("cityName"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        locator.registerListener();
    }

    @Override
    protected void onPause() {
        super.onStop();
        locator.stopListener();
    }

    private void setupInterfaceObjects() {
        weatherOverview = (TextView) findViewById(R.id.weatherStatus);
        baseName = (TextView) findViewById(R.id.baseName);
        weatherOverview.setText("Loading...");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static Context getAppContext(){
        return appContext;
    }

    public static Database getDatabase(){
        return database;
    }

    public static MainActivity getInstance(){
        return instance;
    }
}
