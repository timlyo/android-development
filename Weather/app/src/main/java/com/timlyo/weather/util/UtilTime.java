package com.timlyo.weather.util;

import android.text.format.Time;
import android.util.Log;

import com.timlyo.weather.MainActivity;

public class UtilTime {

    public static Time getTime(){
        Time time = new Time();
        time.setToNow();
        return time;
    }

    public static boolean isTimeForUpdate(Time lastUpdate){
        return UtilTime.isTimeForUpdate(lastUpdate, UtilTime.getTime());
    }

    public static boolean isTimeForUpdate(Time lastUpdate, Time currentTime){
        if(lastUpdate == null) //value may be null on first call
            return true;

        Long time = Math.abs(lastUpdate.toMillis(true) - currentTime.toMillis(true));

        try {
            if (time > 600000) {
                return true;
            }
        }catch (NullPointerException e) {
            e.printStackTrace();
        }
        return false;
    }
}
