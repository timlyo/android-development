package com.timlyo.weather;

import android.content.Context;
import android.content.SharedPreferences;

public class Database {
    private Context context;
    private SharedPreferences sharedPreferences;

    public Database(String name, Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public void saveValue(String key, String value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getValue(String key){
        if(sharedPreferences.contains(key))
            return sharedPreferences.getString(key, null);
        return "nope";
    }
}
