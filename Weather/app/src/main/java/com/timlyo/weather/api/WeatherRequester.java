package com.timlyo.weather.api;

import android.os.AsyncTask;
import android.text.format.Time;
import android.util.Log;

import com.timlyo.weather.MainActivity;
import com.timlyo.weather.util.UtilNet;
import com.timlyo.weather.util.UtilTime;

import java.io.IOException;

public class WeatherRequester extends AsyncTask{
    private Time lastRequestTime;


    //parts of the url for request
    private String apiKey =  "APPID=0f49e4ffee917e89f230191a22ad386e";
    private String urlString = "http://api.openweathermap.org/data/2.5/weather?";
    private String latitude = "lat=52";
    private String longitude = "lon=1";

    //variable to store the api response
    private String response;

    private boolean done = false;

    public String getUrl(){
        return urlString + latitude + "&" + longitude + "&" + apiKey;
    }
    public String getResponse(){
        return response;
    }
    public boolean isDone(){
        return done;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        try {
            getWeather();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    /*
    function to handle the entire process of getting the weather status
     */
    public void getWeather() throws IOException{
        Log.v(MainActivity.TAG, "Downloading from " + getUrl());

        if(UtilTime.isTimeForUpdate(lastRequestTime)) {
            response = UtilNet.getHTTPResponse(getUrl());
            done = true;
            lastRequestTime = UtilTime.getTime();

            Log.v(MainActivity.TAG, "Weather status downloaded");
        }else {
            done = false;
            Log.v(MainActivity.TAG, "Weather status not downloaded");
        }
    }

    /*
    get formatted value of latitude
     */
    public void setLatitude(double latitude){
        this.latitude = "lat=" + Double.toString(latitude);
    }

    /*
    get formatted value of longitude
     */
    public void setLongitude(double longitude){
        this.longitude = "lon=" + Double.toString(longitude);
    }
}
