package com.timlyo.weather.api;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.timlyo.weather.MainActivity;

public class Locator {
    private LocationManager locationManager;
    private LocationListener locationListener;

    private static Location location;

    public Locator(){
        locationManager = (LocationManager) MainActivity.getAppContext().getSystemService(Context.LOCATION_SERVICE);

        setupListener();
        registerListener();
    }

    private void setupListener(){
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Locator.setLocation(location);
                MainActivity.getInstance().updateWeather();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
    }

    public void registerListener(){
        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    public void stopListener(){
        locationManager.removeUpdates(locationListener);
    }

    public static void setLocation(Location location) {
        Locator.location = location;
    }

    public Double getlatitude(){
        return location.getLatitude();
    }

    public Double getLongitude(){
        return location.getLongitude();
    }
}
